# Zapper

El dispositivo Zapper sirve para matar parásitos en el cuerpo humano.
Tambié sirve para electrificar la sangre.

En la versión [v1.1/](v1.1) encontrarás las siguientes información:

* **[01kicad](v1.1/01kicad)**: Circuito esquemático y diseño del PCB.
* **[02termotransferencia](v1.1/02termotransferencia)**: PDF con las capas de cobre y  silkscreen para generar el PCB con la técnica de termotransferencia.
* **[03serigrafia](v1.1/03serigrafia)**: PDF con las capas de cobre, 
silkscreen y soldermask para generar el PCB con la técnica de serigrafía.
* **[04caja](v1.1/04caja)**: PDF y SVG con los planos de la caja para corte 
láser.
* **[05datasheet](v1.1/05datasheet)**: Información del circuito Integrado NE555.

## Componentes

Los componentes que se utilizan en la versión V1.1 del zapper son los siguientes:

* 2 resistores de 1k, colores: café, negro, rojo, dorado
* 2 resistores de 3.9k, colores: naranja, blanco, rojo
* 1 resistor de 4.7k, colores: amarillo, morado, rojo
* 1 capacitor de 10 nF, código: 103
* 1 Capacitor de 4.7 nF, código 472
* 1 Circuito integrado NE555
* 4 conectores molex paso 100
* 1 tablilla de circuito impreso 

Los conectores ya vienen ensamblados y son los siguientes

* 1 conector con interruptor
* 1 Conector con LED
* 1 Conector con broche para pila cuadrada
* 1 Conector con jack

## Herramientas

Las herramientas que se utilizan para soldar los componentes son:

* 1 cautín tipo lápiz de 40 a 60 watts.
* Base para cautín con esponja.
* Soldadura 60/40 de 1 mm de diámetro con núcleo de resina.
* Pinzas de corte.

## Secuencia para soldar los componentes

1. Colocar los resistores de 1k (café negro, rojo, dorado) en R1 y R3.
2. Colocar los resistores de 3.9k (naranja, blanco, rojo, dorado) en R2 y R4.
2. Colocar el resistor de 4.7k (amarillo, morado, tojo, dorado) en R5.
3. Soldar los cinco resistores, cortar los sobrantes y guardar uno de esos sobrantes.
4. Colocar el pin sobrante como un puente entre el espacio de los capacitores C1 y C2, 
Soldar y cortar los sobrantes.
5. Colocar el Circuito integrado NE555 y soldar. Cuida que la marca coincida con el
dibujo de la placa.
6. Colocar el capacitor de 10nF (103) en C1 y el capacitor de 4.7nF (472) en C2.
Soldar y cortar los sobrantes.
7. Colocar la base de los conectores Molex en las siguientes posiciones: J1 (9VCD),
J2 (OUT), J3 (SW), D1 (CLK). Cuida que la orientación del conector coincida con 
el dibujo de la placa.

# Diagrama original

![Diagrama Zapper](v1.1/img/01diagrama.png "Reger, sf,  p 1193")
Tomada de Regehr (sf, p 1193) 

# Esquemático en KiCAD

![Diagrama Esquemático Kicad Zapper](v1.1/img/02esquematicoKicad.png)

# PCB en kicad

![PCB en Kicad Zapper](v1.1/img/03pcbKicad.png)

# 3D en Kicad

![PCB en 3D Zapper](v1.1/img/043dKicad.png)



# Calculo de la frecuencia para el NE555

De acuerdo con la [hoja de datos](https://www.ti.com/lit/ds/symlink/ne555.pdf?ts=1717218974098&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FNE555) 
del NE555, la frecuencia de operación está determinada por la siguiente relación
$$f = \frac{1.44}{(R1+2R2) C2}$$

Es decir
$$f = \frac{1.44}{(1k+2*3.9k) 4.7n} = 34.816 k Hz$$


